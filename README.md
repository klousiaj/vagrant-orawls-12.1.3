#vagrant-orawls-12.1.3

Taken from Edmond Biemond's git repo (https://github.com/biemond/biemond-orawls) and Matthew Baldwin's git repo (https://github.com/matthewbaldwin/vagrant-wls12c-centos64).

## Details
Creates a 12.1.3 WebLogic Server.

The admin console is availble on port 7001. The admin username and password is weblogic/weblogic1

This uses the vagrant plugin to deploy to AWS. The box is deployed [here](https://vagrantcloud.com/klousiaj/centos65-x86_64-puppet). While the box is publicly available, it relies on a private AMI. Other AMIs are entirely untested.

The AMI generated will be an m1.small.

Update the Vangrantfile to reflect the correct path to the software shares. The software share should include:
* jdk-7u51-linux-x64.tar.gz
* fmw_12.1.3.0.0_wls.jar
* UnlimitedJCEPolicyJDK7.zip

##Dependencies
* Java1.7u67 - available from Oracle for download.
* WebLogic 12c - available from Oracle for download.
* Puppet 1.3.1 - Puppet is installed on the AMI.
* Amazon Web Services - an EC2 will be initialized and launched. This particular instance requires a private AMI.
* Java Cryptography Extension - [UnlimitedJCEPolicyJDK7.zip](http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html)

